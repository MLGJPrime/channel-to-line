import os


def convert_layout(input_file, output_file, ext):
    with open(input_file) as f_in:
        start_line = 0
        for i, line in enumerate(f_in):
            line = line.strip().split()
            if all(value.replace('.', '', 1).isdigit() for value in line):
                start_line = i
                break

    with open(input_file, 'r') as f_in, open(output_file, 'w') as f_out:
        # Read lines from input file
        lines = f_in.readlines()

        # Extract file path and write it to output file
        output_file_name = os.path.basename(output_file)
        column_width = len(output_file_name) + 4
        f_out.write(f"{output_file_name:<{column_width}}{ext}\n")

        # Extract and write data values to output file
        channel = 0
        for line in lines[start_line:]:
            if line.strip():  # Check if line is not empty
                values = line.split()
                for value in values:
                    channel += 1
                    f_out.write(f"{channel:<{column_width}}{value}\n")
