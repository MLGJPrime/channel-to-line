# Channel To Line Converter

## Description
The "Channel To Line Converter" is a Python program designed to streamline the process of converting the layout of input files from multiple channels in one line to a new file format where each line corresponds to a single channel. This repository contains the source code for the conversion program.

The program utilizes an Azure custom theme and also has the capability to extract the header of the input file, which typically doesn't contain channels. It offers a simple and intuitive interface where users can either manually input file paths or browse for them. Similarly, users can specify output file paths manually or browse for them. Upon pressing the convert button, the program automatically names and generates the output file in the same directory as the input file.

The program includes an executable version that can be found in the repository. Please note that the executable needs to be whitelisted in the firewall.

## Installation
No installation steps are necessary if using the provided executable. However, if you prefer to run the Python script converter_gui.py, ensure you have Python installed on your system. Clone this repository to your local machine, navigate to the project directory, and run the following command:

python3 converter_gui.py

## Usage
To run the executable program, navigate to the project directory, double click on the file in the File Explorer or execute the following command in your terminal:

.\ChannelToLineConverter.exe

Follow the on-screen instructions to input the file paths and perform the conversion.

## Support
For any assistance or inquiries, please contact Jan Predrag via email at jp8726@student.uni-lj.si.

## Authors and Acknowledgment
This program was developed by Jan Predrag as part of work conducted at the Josef Stefan Institute in Slovenia.

## License
This project is not currently licensed.

## Project Status
Development of this project is ongoing. Contributions and feedback are welcome.
