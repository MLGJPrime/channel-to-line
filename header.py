def extract_header(input_file, output_file):
    with open(input_file, 'r') as f_in, open(output_file, 'w') as f_out:
        for line in f_in:
            # Check if all the words in the line are digits
            if all(value.isdigit() for value in line.split()):
                break
            f_out.write(line)
