import tkinter as tk
from tkinter import filedialog, font
from tkinter import ttk
import os
import sys
from convert import convert_layout
from header import extract_header

# Constants
INPUT_FILE_PROMPT = "Select Input File"
OUTPUT_FILE_PROMPT = "Save As"
DEFAULT_EXTENSION = ".dat"
ALLOWED_EXTENSIONS = ['.rbs', '.nra', '.spe']


# Functions
def select_input_file():
    output_file_entry.delete(0, tk.END)
    input_file_paths = filedialog.askopenfilenames(title=INPUT_FILE_PROMPT)
    if input_file_paths:
        input_file_entry.delete(0, tk.END)
        input_file_entry.insert(0, ', '.join(os.path.basename(path) for path in input_file_paths))  # noqa: E501
        input_file_entry._absolute_paths = input_file_paths


def select_output_file():
    output_file_path = output_file_entry.get()
    if output_file_path:  # If the user has manually entered a path
        input_file_dir = os.path.dirname(
            getattr(input_file_entry, '_absolute_path', '')
        )
        output_file_path = os.path.join(input_file_dir, output_file_path)
    else:  # If the user hasn't manually entered a path, open the file dialog
        output_file_path = filedialog.asksaveasfilename(
            title=OUTPUT_FILE_PROMPT, defaultextension=DEFAULT_EXTENSION
        )
    if output_file_path:
        output_file_entry.delete(0, tk.END)
        output_file_entry.insert(0, os.path.basename(output_file_path))
        output_file_entry._absolute_path = output_file_path


def update_output_file_entry(output_file):
    output_file_entry.delete(0, tk.END)
    output_file_entry.insert(0, os.path.basename(output_file))
    output_file_entry._absolute_path = output_file


def validate_files(input_file, output_file_name):
    if not input_file:
        status_label.config(text="Please select an input file.")
        return False, None

    # If output field empty, set output_file_name to input_file_name-{ext}.dat
    input_file_name, input_extension = os.path.splitext(os.path.basename(input_file))  # noqa: E501
    detector = 'DET'
    if not output_file_name:
        if input_extension.lower() in ALLOWED_EXTENSIONS:
            output_file_name = (
                input_file_name + '-' + input_extension[1:].lower() +
                DEFAULT_EXTENSION
            )
        else:
            output_file_name = input_file_name + DEFAULT_EXTENSION

    for det in ('RBS', 'NRA', 'SPE'):
        if det in os.path.basename(input_file).upper():
            detector = det

    # Check if output file has an extension, if not, add .dat
    _, output_extension = os.path.splitext(output_file_name)
    if not output_extension:
        output_file_name += DEFAULT_EXTENSION

    return output_file_name, detector


def perform_conversion():
    input_files = getattr(input_file_entry, '_absolute_paths', [])
    output_file_name = output_file_entry.get()
    successful_files = []
    error_files = []

    for input_file in input_files:
        output_file_name, ext = validate_files(input_file, output_file_name)

        file_header = os.path.join(
            os.path.dirname(input_file),
            output_file_name.split('.')[0] + '_header.txt'
        )

        # Ensure output file is in the same directory as input file
        output_file = os.path.join(
            os.path.dirname(input_file),
            output_file_name
        )

        try:
            extract_header(input_file, file_header)
            convert_layout(input_file, output_file, ext)
            successful_files.append(os.path.basename(input_file))
        except Exception:
            error_files.append(os.path.basename(input_file))

        # Update the output file entry field with the modified output file name
        update_output_file_entry(output_file)

        output_file_name = ''

    # Update the status label with the status message
    if successful_files:
        success_message = "Successfully converted files:\n" + \
            '\n'.join(successful_files)
    else:
        success_message = "No files were successfully converted."

    if error_files:
        error_message = "Files with errors:\n" + '\n'.join(error_files)
    else:
        error_message = "No files had errors."

    status_label.config(text=success_message + '\n' + error_message)


# Create GUI
root = tk.Tk()
root.title("Text File Layout Converter")

# Create a Style object
style = ttk.Style()

if getattr(sys, 'frozen', False):
    # Running in a bundle (i.e., from the executable)
    theme_path = os.path.join(
        sys._MEIPASS, 'Azure-ttk-theme-main', 'azure.tcl'
    )
else:
    # Running normally
    theme_path = os.path.join('.', 'Azure-ttk-theme-main', 'azure.tcl')

# Use theme_path where you need to access the theme files
light_theme_path = os.path.join(theme_path, 'theme', 'light.tcl')
dark_theme_path = os.path.join(theme_path, 'theme', 'dark.tcl')

root.tk.call("source", theme_path)
root.tk.call("set_theme", "dark")

# Create a font object
custom_font = font.Font(family='Segue UI', size=10)

# Configure the style for Label, Entry, and Button
style.configure('TLabel', font=custom_font)
style.configure('TEntry', font=custom_font)
style.configure('TButton', font=custom_font)

# Input file selection
input_file_label = ttk.Label(root, text="Input File:", style='TLabel')
input_file_label.grid(row=0, column=0, padx=10, pady=5)

input_file_entry = ttk.Entry(root, width=30, style='TEntry')
input_file_entry.grid(row=0, column=1, padx=10, pady=5)

input_file_button = ttk.Button(
    root, text="Browse", command=select_input_file, style='TButton'
)
input_file_button.grid(row=0, column=2, padx=10, pady=5)

# Output file selection
output_file_label = ttk.Label(root, text="Output File:", style='TLabel')
output_file_label.grid(row=1, column=0, padx=10, pady=5)

output_file_entry = ttk.Entry(root, width=30, style='TEntry')
output_file_entry.grid(row=1, column=1, padx=10, pady=5)

output_file_button = ttk.Button(
    root, text="Browse", command=select_output_file, style='TButton'
)
output_file_button.grid(row=1, column=2, padx=10, pady=5)

# Convert button
convert_button = ttk.Button(
    root, text="Convert", command=perform_conversion, style='TButton'
)
convert_button.grid(row=2, column=1, pady=10)

# Status label with word wrap
status_label = ttk.Label(root, text="", wraplength=300, style='TLabel')
status_label.grid(row=3, column=1)

root.mainloop()
